module.exports = function(grunt) {

    grunt.initConfig({
        shell: {
				options: {
						stdout: true,
						stderr: true
				},
				server: {
						command: 'python manage.py runserver 8000'
				}
		},

        watch: {
            server: {
                files: [
                    'static/js/**',
                    'static/css/**/*.css'
                ],
                options: {
                    livereload: true
                }
            }
        },

        concurrent: {
			target: ['watch', 'shell'],
			options: {
				logConcurrentOutput: true
			}
		}
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['concurrent']);
};
