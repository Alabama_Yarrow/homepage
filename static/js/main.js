'use strict';

require.config({
    paths: {
        jquery: 'lib/jquery-2.1.4',
		underscore: 'lib/underscore',
		backbone: 'lib/backbone',
        handlebars: 'lib/handlebars',
        text: 'lib/text'
    },

    shim: {
        underscore: {
            exports: '_' //creates window._ on underscore load

        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone' //creates window.Backbone on backbone load
        },
        handlebars: {
            exports: 'Handlebars'
        }
    }
});

define([
    'backbone', 'router'
    ], function(
    Backbone
){
    Backbone.history.start();
});





