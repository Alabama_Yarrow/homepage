define(['backbone',
    'handlebars',
    'views/about',
    'views/contact',
    'views/portfolio',
    'views/home'
], function(Backbone, Handlebars, aboutView, contactView, portfolioView, homeView) {

    var Router = Backbone.Router.extend({

        initialize: function () {
            var page = $('body');
            page.append(aboutView.el);
            page.append(contactView.el);
            page.append(homeView.el);
            page.append(portfolioView.el);
            aboutView.hide();
            contactView.hide();
            homeView.hide();
            portfolioView.hide();
        },

        routes: {
            'about': 'about',
            'contact': 'contact',
            'portfolio': 'portfolio',
            '*default': 'home'
        },

        about: function () {
            this.changeView(aboutView);
        },

        contact: function () {
            this.changeView(contactView);
        },

        home: function () {
            this.changeView(homeView);
        },

        portfolio: function () {
            this.changeView(portfolioView);
        },

        changeView: function(newView) {
            if(this.currentView) {
                this.currentView.hide();
            }
            this.currentView = newView;
            this.currentView.show();
        }
    });

    return new Router();
});