define([
    'backbone',
    'handlebars',
    'text!tmpl/contact.html'
    ], function(
        Backbone,
        Handlebars,
        template
    ) {

    var View = Backbone.View.extend({
        template: Handlebars.compile(template),
        tagName: 'section',
        className: 'backing',

        initialize: function() {
            this.render();
        },

        render: function() {
            this.$el.html(this.template);
        },

        show: function() {
            this.$el.show()
        },

        hide: function() {
            this.$el.hide()
        }
    });

    return new View();
});